#!/usr/bin/env python3

import math

import matplotlib.cm
import matplotlib.patches
import matplotlib.pyplot

# PHI = (1 + math.sqrt(5)) / 2

if __name__ == "__main__":
    matplotlib.pyplot.rc("text", usetex=True)
    matplotlib.pyplot.rc("font", family="serif")

    colours = matplotlib.cm.tab10(range(2))

    width = 6.4
    fig, ax = matplotlib.pyplot.subplots(
        tight_layout=True,
        figsize=(width, width / 3),
    )

    legend_labels = (
        r"\texttt{NJet}",
        "Model",
    )

    y_ticklabels = (
        "VEGAS",
        "Baseline",
        r"$p_{T,\gamma} > 50\,\mathrm{GeV}$",
        r"$m_{\gamma\gamma} > 50\,\mathrm{GeV}$",
    )

    y_ticks = range(0, len(y_ticklabels))

    ax.set_yticks(
        ticks=y_ticks,
        labels=y_ticklabels,
    )

    # 6pt: ((4.9, 0.5), (4.5, 0.6)),

    data = (
        ((4.151, 0.011), (4.22, 0.08)),
        ((4.149, 0.006), (4.19, 0.07)),
        ((0.5283, 0.0008), (0.54, 0.02)),
        ((3.300, 0.005), (3.34, 0.05)),
    )

    y_sep = 0.1

    for legend_label, trans, shift, colour, alpha in zip(
        legend_labels,
        zip(*data),
        (1, -1),
        colours,
        (0.4, 0.2),
    ):
        for (x, xerr), y in zip(trans, y_ticks):
            r = matplotlib.patches.Rectangle(
                xy=((x - xerr) * 1e-6, y - y_sep * 2),
                width=2 * xerr * 1e-6,
                height=y_sep * 4,
                alpha=alpha,
                facecolor=colour,
            )
            ax.add_patch(r)

        ax.errorbar(
            [t[0] * 1e-6 for t in trans],
            [y + y_sep * shift for y in y_ticks],
            xerr=[t[1] * 1e-6 for t in trans],
            label=legend_label,
            linestyle="",
            marker=".",
        )

    ax.legend(
        loc="best",
        frameon=True,
    )
    ax.set_xlabel(r"$\sigma_{gg\to\gamma\gamma g}$ [pb]")

    fig.savefig("img/xs.pdf")
