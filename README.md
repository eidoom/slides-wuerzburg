# [slides-wuerzburg](https://gitlab.com/eidoom/slides-wuerzburg)

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7759561.svg)](https://doi.org/10.5281/zenodo.7759561)

[Live here](https://eidoom.gitlab.io/slides-wuerzburg/slides.pdf)

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
