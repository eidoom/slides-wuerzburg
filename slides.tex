\documentclass[]{beamer}

\usetheme{Singapore}

\usepackage{caption}
\usepackage{cleveref}
\usepackage{siunitx}
\usepackage{tikz}
\usetikzlibrary{}
\usepackage{pgfplots}

\newcommand{\medium}[1]{#1!40}

\usepackage[
    bibstyle=authoryear,
    citestyle=authoryearsquare,
    maxcitenames=4,
]{biblatex}
\addbibresource{bibliography.bib}
\renewcommand*{\bibfont}{\scriptsize}

\graphicspath{{./img/}}

\usefonttheme[onlymath]{serif}

\title{Optimising hadron collider simulations using amplitude neural networks}
\author{Ryan Moodie}
\institute{Durham University and Turin University \\\vspace{3ex} with Joseph Aylett-Bullock and Simon Badger}
\date{W\"urzburg particle theory seminars \\\vspace{1ex} 9 Jun 2022}
\titlegraphic{
    \begin{tikzpicture}[x=4.5em]
        \node at (0,0) {\includegraphics[width=3em,height=3em,keepaspectratio]{stfc_logo}};
        \node at (1,0) {\includegraphics[width=3em,height=3em,keepaspectratio]{du_logo}};
        \node at (2,0) {\includegraphics[width=4em,height=3em,keepaspectratio]{ippp_logo}};
        \node at (3,0) {\includegraphics[width=3em,height=3em,keepaspectratio]{infn_logo}};
        \node at (4,0) {\includegraphics[width=3em,height=3em,keepaspectratio]{torino_logo}};
        \node at (5,0) {\includegraphics[width=3em,height=3em,keepaspectratio]{erc_logo}};
    \end{tikzpicture}
}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}{Outline}{Optimising hadron collider simulations using amplitude neural networks}
    \tableofcontents
\end{frame}

\section[Diphoton pheno]{Diphoton phenomenology}
\label{sec:intro}

\begin{frame}{Outline}{Optimising hadron collider simulations using amplitude neural networks}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Precision phenomenology}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Indirect probe of new physics by small deviations from SM
                \item Modern collider experiments demand increasing precision
                \item Percent-level QCD theory predictions
                \item Requires $\ge$ NNLO
                    \begin{align*}
                        \alpha_s&\approx 0.1\\
                        \mathrm{d}\sigma&=\mathrm{d}\sigma^{\mathrm{LO}}+\alpha_s \mathrm{d}\sigma^{\mathrm{NLO}} + {\alpha_s}^2 \mathrm{d}\sigma^{\mathrm{NNLO}}
                    \end{align*}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{jets_v1}
                \captionsetup{labelformat=empty}
                \caption{\parencite{collaboration:2114784}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Diphoton final states}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Flurry of activity
                \item Important background for measuring Higgs properties
                \item Rich $2\to3$ kinematics offer attractive probes
                \item Gluon fusion
                    \begin{itemize}
                        \item Excellent testing ground for new technologies
                    \end{itemize}
                \item Loop-induced
                    \begin{itemize}
                        \item Challenges conventional phase space optimisations
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=0.8\textwidth]{ggyyg_1l}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Perturbative anatomy}
    \begin{figure}
        \includegraphics[width=0.9\textwidth]{perturbative}
    \end{figure}
\end{frame}

\begin{frame}{Hadron collider simulations}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Complicated
                    \begin{itemize}
                        \item[\textcolor{\medium{green}}{\textbullet}] High-multiplicity amplitudes
                        \item[\textcolor{\medium{red}}{\textbullet}] PDFs
                        \item Variable centre-of-mass scales
                        \item Phase space sampling
                        \item Phase space cuts
                        \item Jet clustering
                    \end{itemize}
                \item Expensive
                    \begin{itemize}
                            \vspace{1ex}
                        \item[]
                            \begin{tabular}{l r}
                                \hline
                                Correction & CPU hours \\
                                \hline
                                Virtual & \num{50000} \\
                                Real & \num{500000} \\
                                \hline
                            \end{tabular}
                            \vspace{2ex}
                        \item[] \parencite{badger:2021ohm}
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \hspace*{-3em}
                \includegraphics[width=1.3\textwidth]{collision}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{Machine learning}
\label{sec:ann}

\begin{frame}{Outline}{Optimising hadron collider simulations using amplitude neural networks}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Neural networks}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \begin{center}
                    \begin{tikzpicture}[scale=0.35]
                        \begin{axis}[
                                xmin=-2,
                                xmax=2,
                                ymin=-2,
                                ymax=2,
                                xlabel=$x$,
                                ylabel=$\tanh(x)$,
                                axis lines=center,
                                x label style={at={(axis description cs:1.0,0.52)},anchor=south},
                                y label style={at={(axis description cs:0.42,1.0)},anchor=east},
                            ]
                            \addplot[
                                color=blue!35,
                                ultra thick,
                                samples=100,
                            ] plot[smooth] {tanh(x)};
                        \end{axis}
                    \end{tikzpicture}
                    \begin{tikzpicture}[scale=0.35]
                        \begin{axis}[
                                xmin=-2,
                                xmax=2,
                                ymin=-2,
                                ymax=2,
                                xlabel=$x$,
                                ylabel=$\mathrm{ReLU}(x)$,
                                axis lines=center,
                                x label style={at={(axis description cs:1.0,0.52)},anchor=south},
                                y label style={at={(axis description cs:0.42,1.0)},anchor=east},
                            ]
                            \addplot[
                                color=blue!35,
                                ultra thick,
                            ] coordinates {(-2,0)(0,0)(2,2)};
                        \end{axis}
                    \end{tikzpicture}
                \end{center}
            \end{figure}
            \vspace{1em}
            \begin{equation*}
                n^{(\ell)}_j = \underbrace{\strut f^{(\ell)}}_{\substack{\text{Activation}\\\text{function}}}\Big( \underbrace{\strut w^{(\ell)}_{ji}}_{\text{Weight}} \underbrace{\strut n^{(\ell-1)}_i}_{\text{Node}} + \underbrace{\strut b^{(\ell)}}_{\text{Bias}} \Big)
            \end{equation*}
            \vspace{0.5em}
            \begin{center}
                \textcolor{blue!70!black}{\textbullet} Classification \hspace{2em} \textcolor{blue!70!black}{\textbullet} Regression
            \end{center}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{nn}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Supervised learning}
    \begin{description}[leftmargin=10em]
        \item[Neural network]
            \begin{itemize}
                \item $f:\mathbb{R}^{d_i}\to\mathbb{R}^{d_o}$
            \end{itemize}
        \item[Target dist.]
            \begin{itemize}
                \item $\left\{ (x_i,y_i) \mid {\small i\in\{1,\ldots,n\}} \right\}$
                \item Training data
            \end{itemize}
        \item[Loss function]
            \begin{itemize}
                \item Mean-squared error
                \item $L=\frac{1}{n}\sum_{i=1}^{n}\left(f(x_i)-y_i\right)^2$
            \end{itemize}
        \item[Optimise]
            \begin{itemize}
                \item Weights and biases
                \item Minimise $L$
                \item Gradient descent
                \item Backpropagation
            \end{itemize}
    \end{description}
\end{frame}

\begin{frame}{Applications in event generators}
    \begin{description}
        \item[Phase space sampling]
            Importance sampling multi-channel mappings, phase space event distributions
        \item[Parton distribution functions]
            Fitting from data, \texttt{NNPDF}
        \item[Loop integrals]
            Integration contour in sector decomposition
        \item[Matrix elements]
            Emulation
        \item[Parton shower]
            Markov chain models, splitting kernel fitting
        \item[Fragmentation functions]
            Model-independent fitting
        \item[End-to-end generation]
        \item[\ldots]
            \parencite{feickert:2021ajf}
    \end{description}
\end{frame}

\begin{frame}{Amplitude emulation}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Approximate matrix elements with neural networks
                    \begin{itemize}
                        \item Train models using amplitude libraries
                        \item Tame IR behaviour
                        \item Apply in realistic hadron collider simulations
                    \end{itemize}
                \item[] \parencite{aylett-bullock:2021hmo}, \parencite{moodie:2022flt}
                \item[] \url{https://gitlab.com/JosephPB/n3jet_diphoton}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=0.8\textwidth]{ggyygg_1l}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{Amplitude neural networks}
\label{sec:proj}

\begin{frame}{Outline}{Optimising hadron collider simulations using amplitude neural networks}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Amplitudes}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Use amplitudes from \texttt{NJet}
                    \begin{itemize}
                        \item[] \parencite{badger:2012pg}
                    \end{itemize}
                \item Numerical
                    \begin{itemize}
                        \item Integrand reduction, generalised unitarity
                        \item Automated but slow
                    \end{itemize}
                \item Analytical
                    \begin{itemize}
                        \item High multiplicity unavailable
                    \end{itemize}
                \item Neural network model
                    \begin{itemize}
                        \item Favourable time scaling
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{timing-single}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Data generation}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Extract from event generator
                \item Training
                    \begin{itemize}
                        \item 100k points
                        \item 4:1 training and validation sets
                    \end{itemize}
                \item Testing
                    \begin{itemize}
                        \item 3M points
                        \item Test with different random number seed
                    \end{itemize}
                \item Standardise input and output variable distributions
                    \begin{itemize}
                        \item Mean of 0
                        \item Standard deviation of 1
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{data_generation2}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Infrared behaviour}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Infrared divergences from soft $ s_i $ and collinear $ c_{ij} $ emissions
                \item Local extreme curvature spoils global fit
                \item Naive model struggles
                \item Ensemble
                    \begin{itemize}
                        \item One network for each divergent structure
                        \item[] \parencite{badger:2020uow}
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{unit_error_plot_joint}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Phase space partitioning}
    \vspace{-1em}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Partition phase space
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{ir_partition_hor}
            \end{figure}
        \end{column}
    \end{columns}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item[\textcolor{\medium{cyan}}{\textbullet}] Tune threshold
                    \begin{itemize}
                        \item Each region contains points of similar scales
                        \item Sufficient points in \textcolor{red}{$ \mathcal{R}_{\text{div}} $}
                        \item Higher $y_\text{cut}$ more expensive
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.8\textwidth]{y_cut_tuning}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Subdivision of \texorpdfstring{\textcolor{red}{$ \mathcal{R}_{\text{div}} $}}{divergent region}}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Subdivide \textcolor{red}{$ \mathcal{R}_{\text{div}} $} using FKS
                    \begin{itemize}
                        \item[] \parencite{frederix:2009yq}
                    \end{itemize}
                \item $ \mathcal{P}_{\text{FKS}} = \left\lbrace (i,j) \mid s_i \lor s_j \lor c_{ij} \right\rbrace $
                    \vspace{0.5ex}
                \item $ \mathcal{S}_{ij} = 1 / \left( s_{ij} \sum_{k,\ell\in \mathcal{P}_{\text{FKS}}} \frac{1}{s_{k\ell}} \right) $
                    \begin{itemize}
                            \vspace{1ex}
                        \item Partition functions smoothly isolate singularities
                            \vspace{1ex}
                    \end{itemize}
                \item $ \lvert\mathcal{A}\rvert^2 = \sum_{i,j\in \mathcal{P}_{\text{FKS}}} \underbrace{\mathcal{S}_{ij} \lvert\mathcal{A}\rvert^2}_{\text{Network}} $
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=0.8\textwidth]{blobs}
            \end{figure}
            \begin{figure}
                \includegraphics[width=\textwidth]{ir_ensemble}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Network architecture}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item General architecture
                    \begin{itemize}
                        \item Ensemble, partition, process, cuts
                        \item Hyperparameter optimisation on $gg\rightarrow\gamma\gamma g$
                    \end{itemize}
                \item Fully-connected neural network
                    \begin{itemize}
                        \item \texttt{Keras} with \texttt{TensorFlow} backend
                        \item[\textcolor{\medium{green}}{\textbullet}] $ n \times 4 $ input nodes
                        \item[\textcolor{\medium{blue}}{\textbullet}] 20-40-20 hidden nodes (hyperbolic-tangent)
                        \item[\textcolor{\medium{red}}{\textbullet}] Single output node (linear)
                    \end{itemize}
                \item Training by gradient descent
                    \begin{itemize}
                        \item Mean-squared-error loss function
                        \item Adam optimisation
                        \item Training epochs by Early Stopping
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{neural_network}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Uncertainties}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Infer on ensemble of 20 models
                    \begin{itemize}
                        \item Different random weight initialisation
                        \item Shuffled data sets
                    \end{itemize}
                \item Result
                    \begin{itemize}
                        \item Mean
                        \item Standard error (precision/optimality errors)
                    \end{itemize}
                \item Alternative: Bayesian networks
                    \begin{itemize}
                        \item[] \parencite{kasieczka:2020vlh}
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=0.8\textwidth]{stochastic_ensemble}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Interfacing with event generators}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Training in \texttt{Python}
                    \begin{itemize}
                        \item[\textcolor{\medium{blue}}{\textbullet}] Event generators in \texttt{C++}
                    \end{itemize}
                \item[\textcolor{\medium{orange}}{\textbullet}] \texttt{C++} inference code
                    \begin{itemize}
                        \item[\textcolor{\medium{orange}}{\textbullet}] Linear algebra by \texttt{Eigen3}
                    \end{itemize}
                \item[\textcolor{\medium{blue}}{\textbullet}] Event generator: \texttt{Sherpa}
                    \begin{itemize}
                        \item[] \parencite{bothmann:2019yzt}
                    \end{itemize}
                \item[\textcolor{\medium{green}}{\textbullet}] \texttt{C++} interface with \texttt{Sherpa}
                \item[\textcolor{\medium{blue}}{\textbullet}] \texttt{LHAPDF}, \texttt{NNPDF31\_nlo\_as\_0118}, \texttt{Rivet}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{interface2}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Pipeline overview}
    \begin{figure}
        \includegraphics[width=\textwidth,height=0.8\textheight,keepaspectratio]{pipeline2}
    \end{figure}
\end{frame}

\section{Testing}
\label{sec:res}

\begin{frame}{Outline}{Optimising hadron collider simulations using amplitude neural networks}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Methodology}
    \begin{description}
        \item[Process]
            \begin{itemize}
                \item $ gg \to \gamma \gamma g$
                \item $ gg \to \gamma \gamma gg$
            \end{itemize}
        \item[Agreement]
            \begin{itemize}
                \item Matrix element
                \item Total cross section
                \item Differential cross section
            \end{itemize}
        \item[Errors]
            \begin{itemize}
                \item \texttt{NJet}: Monte Carlo
                \item Model: precision/optimality
            \end{itemize}
        \item[$ gg \to \gamma \gamma g$]
            \begin{itemize}
                \item Cuts
                \item Integrators
            \end{itemize}
    \end{description}
\end{frame}

\begin{frame}{Cuts and parameters}
    \begin{itemize}
        \item Typical diphoton LHC cuts
        \item[]
            \begin{align*}
                p_{T,j} &> \SI{20}{\GeV} & R_{\gamma j} &> 0.4 & |\eta_j| &< 5 \\
                p_{T,\gamma_1} &> \SI{40}{\GeV} & R_{\gamma \gamma} &> 0.4 & |\eta_{\gamma}| &< 2.37 \\
                p_{T,\gamma_2} &> \SI{30}{\GeV} & & & & \\
            \end{align*}
        \item Jets with anti-$k_T$ (\texttt{FastJet})
        \item Smooth photon isolation with $R=0.4$ and $\epsilon=0.05$
        \item $\sqrt{s_\text{hadronic}} = \SI{1}{\TeV}$
        \item $\mu_R = m_Z$
    \end{itemize}
\end{frame}

\section{\texorpdfstring{$ gg \to \gamma \gamma g $}{gg->yyg}}

\begin{frame}{Outline}{Optimising hadron collider simulations using amplitude neural networks}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Per-point agreement}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Error distribution sufficiently
                    \begin{itemize}
                        \item Narrow
                        \item Symmetric
                        \item Unit-centred
                    \end{itemize}
                \item Slight tail
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{unit_error_plot}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Integrators}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Integrator
                    \begin{itemize}
                        \item Unit grid
                        \item VEGAS
                    \end{itemize}
                \item Similar performance
                    \begin{itemize}
                        \item Slightly broader
                        \item More points in \textcolor{red}{$ \mathcal{R}_{\text{div}} $}
                    \end{itemize}
                \item Robust to adaptive sampling
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{unit_error_plot}
            \end{figure}
            \begin{figure}
                \includegraphics[width=\textwidth]{vegas_error_plot}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Shifted mean}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Gaussian error distribution
                    \begin{itemize}
                        \item Shifted mean
                    \end{itemize}
                \item \textcolor{red}{$ \mathcal{R}_{\text{div}} $} difficult to fit
                \item Gluon PDF suppresses these errors in cross section
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{unit_error_plot}
            \end{figure}
            \begin{figure}
                \includegraphics[width=\textwidth]{x2-error-plot}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Total cross section and cuts}
    \begin{figure}
        \includegraphics[width=\textwidth]{xs}
    \end{figure}
    \begin{itemize}
        \item Overlapping results
    \end{itemize}
\end{frame}

\begin{frame}{Aside}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Improved tree-level descriptions using factorisation
                \item[] \parencite{maitre:2021uaa}
            \end{itemize}
            \begin{figure}
                \includegraphics[width=\textwidth]{factorisation-arch}
            \end{figure}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{n3jet_comparison_inset}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Distributions}
    \begin{columns}
        \begin{column}{0.33\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{five/deta_yy}
            \end{figure}
            \begin{figure}
                \includegraphics[width=\textwidth]{five/dm_yy}
            \end{figure}
        \end{column}
        \begin{column}{0.33\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{five/dpt_j1}
            \end{figure}
            \begin{figure}
                \includegraphics[width=\textwidth]{five/dpt_j2}
            \end{figure}
        \end{column}
        \begin{column}{0.33\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{five/dphi_jj}
            \end{figure}
            \begin{figure}
                \includegraphics[width=\textwidth]{five/dr_jy}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{\texorpdfstring{$ gg \to \gamma \gamma g g $}{gg->yygg}}

\begin{frame}{Outline}{Optimising hadron collider simulations using amplitude neural networks}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Per-point and total cross section}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Much more expensive process
                    \begin{itemize}
                        \item Numerical
                        \item $2\to4$ phase space
                    \end{itemize}
                \item Distribution
                    \begin{itemize}
                        \item Broader
                        \item More shifted
                    \end{itemize}
                \item Total cross section in agreement
                    \begin{itemize}
                        \item $ \sigma_\mathrm{NN} = (4.5 \pm 0.6) \times 10^{-6} \thinspace \mathrm{pb} $
                        \item $ \sigma_\mathrm{NJet} = (4.9 \pm 0.5) \times 10^{-6} \thinspace \mathrm{pb} $
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{six/error-plot-E-0.001-lin}
            \end{figure}
            \begin{figure}
                \includegraphics[width=\textwidth]{six/error-plot-E-0.001-log}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Distributions}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{dr_jy}
            \end{figure}
            \begin{itemize}
                \item Excellent Agreement
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{dpt_j1}
            \end{figure}
            \begin{itemize}
                \item Fluctuations are statistical
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{Conclusion}
\label{sec:conc}

\begin{frame}{Outline}{Optimising hadron collider simulations using amplitude neural networks}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}{Summary}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Extend pioneering work on amplitude models to full hadron collider simulations
                    \begin{itemize}
                        \item Loop-induced $ gg \to \gamma\gamma + \text{jets} $
                        \item FKS partitioned phase space
                        \item Interface to \texttt{Sherpa}
                    \end{itemize}
                \item \textit{Provide general framework for optimising high-multiplicity observables}
                    \vspace{-1em}
                    \begin{itemize}
                        \item Excellent agreement in distributions
                        \item Simulation speed-up: $ N_{\text{infer}} / N_{\text{train}} $
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.25\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{deta_yy}
                \includegraphics[width=\textwidth]{dphi_jj}
            \end{figure}
        \end{column}
        \begin{column}{0.25\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{dm_yy}
                \includegraphics[width=\textwidth]{dpt_j2}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[allowframebreaks]{Bibliography}
    \printbibliography
\end{frame}

\end{document}
